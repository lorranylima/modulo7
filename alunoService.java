package br.com.mentorama.Cadastro;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
@Service
public class CadastroAlunoService  {

    @Autowired
    private CadastroRepository cadastroRepository;

    public List<CadastroAluno> findAll(){
       return cadastroRepository.findAll();
    }

    public List<CadastroAluno> listByName(String nome){
        return cadastroRepository.listByName(nome);
    }

    public Integer post(CadastroAluno cadastroAluno){
        if (cadastroAluno.getId() == null) {
            cadastroAluno.setId(cadastroRepository.count() + 1);
        }
        cadastroRepository.post(cadastroAluno);
        return cadastroAluno.getId();
    }
    public ResponseEntity<String> get1(int id, String nome) {
        try {
            throw new NotfoundException();
        } catch (NotfoundException e) {
            return new ResponseEntity<>("Aluno não existente", HttpStatus.NOT_FOUND);
        }